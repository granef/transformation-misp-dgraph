#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Application to transform MISP events to RDF and schema files used for bulk data import into the Dgraph database.
"""


import sys          # Common system functions
import os           # Common Operating system functions
import argparse     # Arguments parser
import logging, coloredlogs         # Standard logging functionality with colours functionality
from io import TextIOWrapper        # File handler type
import json         # Common JSON parser
import textwrap     # dedent() function to trim multiline strings
import hashlib      # hash operations
from urllib.parse import urlparse   # URL parsing and domain extraction


def generate_schema(schema_file: TextIOWrapper) -> None:
    """Generate and save schema for IoC links and MISP events.

    Args:
        schema_file (TextIOWrapper): File handler to save the generated schema
    """
    schema = """\
        source: string @index(exact, term) .
        ioc: [uid] @reverse .
        ioc.type: string .
        ioc.value: string .
        ioc.misp: [uid] @reverse .
        misp.id: int .
        misp.info: string .
        misp.timestamp: dateTime .
        misp.url: string .

        type Ioc {
            ioc.type
            ioc.value
            ioc.misp
            <~ioc>
        }

        type Misp {
            misp.id
            misp.info
            misp.timestamp
            misp.url
            <~ioc.misp>
        }   
    """
    schema_file.write(textwrap.dedent(schema))


def generate_ioc_link_nodes(zeek_directory: str, source_name: str, rdf_file: TextIOWrapper) -> None:
    """Generation of IoC nodes connected to regular nodes produced by "Transformation-Zeek-Dgraph" module.

    Nodes with attributes relevant to IoC will get a new edge "<ioc>" that will represent a link node for
    IoC nodes. Each link node has an id similar to the IoC value to ease the IoC info connection.

    Following IoC link nodes are created:
        - conn: orig_h, resp_h
        - files: md5, sha1, sha256
        - http: hostname
        - dns: hostname
        - ssl: hostname

    Args:
        zeek_directory (str): Path to directory with extracted Zeek logs
        source_name (str): Name of the data source
        rdf_file (TextIOWrapper): File handler to save the generated RDF records
    """
    for log_name in ["conn", "files", "http", "dns", "ssl"]:
        try:
            parser = __import__("zeek_parsers." + log_name, fromlist=[''])
            with open(zeek_directory + "/" + log_name + ".log", 'r', encoding='utf-8') as log_file:
                for log_line in log_file:
                    rdf_file.write(parser.get_mutations(json.loads(log_line), source_name))
        except ModuleNotFoundError:
            logging.warning("Mutations for '" + log_name + ".log' are not defined")
        except FileNotFoundError:
            logging.warning("Log file '" + log_name + ".log' not found")


def get_misp_ioc_id(ioc_type: str, value: str) -> str:
    """Generate Misp node id according to given IoC type and value corresponding to IoC link nodes ids.

    Types definition: https://www.circl.lu/doc/misp/categories-and-types/

    Args:
        ioc_type (str): IoC type definition
        value (str): IoC value

    Returns:
        str: Generated ID of given IoC for defined IoC types, None otherwise.
    """
    value_split = value.split("|")
    if ioc_type in ["md5", "sha1", "sha256", "ip-dst", "ip-dst|port", "ip-src", "ip-src|port"]:
        # TODO: Currently skips IP subnet definition
        if "/" in value_split[0]:
            return None
        return "ioc-" + value_split[0]
    elif ioc_type in ["filename|md5", "filename|sha1", "filename|sha256", "domain|ip"]:
        return "ioc-" + value_split[1]
    elif ioc_type in ["hostname", "hostname|port", "domain", "domain|ip"]:
        return "ioc-" + hashlib.sha1(value_split[0].replace(':', '_0').encode('utf-8')).hexdigest()
    if ioc_type in ["url"]:
        parsed_url = urlparse(value_split[0])
        uri = parsed_url.path.replace('\n', '').replace('\t', '').replace('\r', '').replace('"', '\'').replace('\\', '\\\\')
        hostname_uri_hash = hashlib.sha1((parsed_url.netloc + uri).replace(':', '_0').encode('utf-8')).hexdigest()
        return "ioc-" + hostname_uri_hash
    else:
        return None


def get_misp_iocs_mutations(event_json: dict, source_name: str) -> str:
    """Extract IoCs from the given event and transform them to nodes.

    Args:
        event_json (dict): Loaded JSON with an event line from MISP export
        source_name (str): Name of the data source

    Returns:
        str: Full nquads definition for given MISP IoC event.
    """
    nquads_mutation = ""
    for ioc in event_json["iocs"]:
        ioc_id = get_misp_ioc_id(ioc.get("type"), ioc.get("value"))
        if ioc_id:
            nquads_mutation += textwrap.dedent("""\
                _:misp-{misp_id} <source> "{data_source}" .
                _:misp-{misp_id} <dgraph.type> "Misp" .
                _:misp-{misp_id} <misp.id> "{misp_id}" .
                _:misp-{misp_id} <misp.info> "{misp_info}" .
                _:misp-{misp_id} <misp.timestamp> "{misp_timestamp}" .
                _:misp-{misp_id} <misp.url> "{misp_url}" .
                _:{ioc_id} <ioc.misp> _:misp-{misp_id} .
            """.format(
                data_source=source_name,
                misp_id=event_json.get("id", ""),
                misp_info=event_json.get("info", "").replace('\\', '\\\\').replace('"', '').replace(':', '_0'),
                misp_timestamp=event_json.get("timestamp", "1970-01-01T00:00:00.000000Z"),
                misp_url=event_json.get("url", ""),
                ioc_id=ioc_id
            ))
    return textwrap.dedent(nquads_mutation)


def generate_misp_iocs(events_directory: str, source_name: str, rdf_file: TextIOWrapper) -> None:
    """Parse extracted IoCs and transform them into the RDF file.

    Args:
        events_directory (str): Path to directory with extracted MISP event files
        source_name (str): Name of the data source
        rdf_file (TextIOWrapper): File handler to save the generated RDF records
    """
    # List and store info about jsonl files in given event files directory
    event_file_names = [f for f in os.listdir(events_directory) if f.endswith('.jsonl')]
    for event_file_name in event_file_names:
        with open(events_directory + "/" + event_file_name, 'r', encoding='utf-8') as event_file:
            for event_line in event_file:
                rdf_file.write(get_misp_iocs_mutations(json.loads(event_line), source_name))


if __name__ == "__main__":
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    parser.add_argument("-or", "--output_rdf", metavar="FILE_NAME", help="Output RDF file",
                        type=argparse.FileType('w', encoding='utf-8'), required=False, default="misp-dgraph.rdf")
    parser.add_argument("-os", "--output_schema", metavar="FILE_NAME", help="Output schema file",
                        type=argparse.FileType('w', encoding='utf-8'), required=False, default="misp-dgraph.schema")
    parser.add_argument("-s", "--source", metavar="CASE_NAME", help="Case name to store as a source to each node", required=False, default="granef", type=str)
    parser.add_argument("-ed", "--events_directory", metavar="DIRECTORY_PATH", help="Path to directory with MISP event files", required=True)
    parser.add_argument("-zd", "--zeek_directory", metavar="DIRECTORY_PATH", help="Path to directory with extracted Zeek logs", required=False)
    parser.add_argument("-l", "--log", choices=["debug", "info", "warning", "error", "critical"], help="Log level", required=False, default="INFO")
    args = parser.parse_args()

    # Set logging
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt='%(asctime)s [%(levelname)s]: %(message)s')

    # Check if the given ecents directory contains any *.jsonl file
    if not any(File.endswith(".jsonl") for File in os.listdir(args.events_directory)):
        logging.critical("Directory '" + args.events_directory + "' doesn't contain any *.jsonl file")
        sys.exit(1)

    logging.info("Generating schema file")
    generate_schema(args.output_schema)

    logging.info("Generating IoC link nodes")
    generate_ioc_link_nodes(args.zeek_directory, args.source_name, args.output_rdf)

    logging.info("Generating MISP IoC relations")
    generate_misp_iocs(args.events_directory, args.source_name, args.output_rdf)
