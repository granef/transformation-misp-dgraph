#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Module to parse files.log file and create related IoC nodes and edges.
"""


import textwrap     # dedent() function to trim multiline strings
import hashlib      # hash operations


def get_mutations(files_json: dict, source_name: str) -> str:
    """Parsing of the files log JSON record and definition of a IoC nodes connected to it.

    Args:
        files_json (dict): Loaded JSON with a files log line from the Zeek log
        source_name (str): Name of the data source

    Returns:
        str: Full nquads definition for given files node.
    """
    id_file = hashlib.sha1(files_json.get("sha1", "").encode('utf-8')).hexdigest()

    nquads_mutation = textwrap.dedent("""\
        _:{id_file} <source> "{data_source}" .
        _:{id_file} <dgraph.type> "File" .
    """.format(
        id_file=id_file,
        data_source=source_name
    ))
    for hash_name in ["md5", "sha1", "sha256"]:
        if hash_name in files_json:
            nquads_mutation += textwrap.dedent("""\
                _:{id_file} <ioc> _:ioc-{hash} .
                _:ioc-{hash} <source> "{data_source}" .
                _:ioc-{hash} <dgraph.type> "Ioc" .
                _:ioc-{hash} <ioc.type> "{hash_name}" .
                _:ioc-{hash} <ioc.value> "{hash}" .
            """.format(
                id_file=id_file,
                hash_name=hash_name,
                data_source=source_name,
                hash=files_json.get(hash_name)
            ))
    return textwrap.dedent(nquads_mutation)
