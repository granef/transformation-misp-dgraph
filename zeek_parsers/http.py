#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Module to parse http.log file and create related IoC nodes and edges.
"""


import textwrap     # dedent() function to trim multiline strings
import hashlib      # hash operations


def get_mutations(http_json: dict, source_name: str) -> str:
    """Parsing of the http log JSON record and definition of a IoC nodes connected to it.

    Args:
        http_json (dict): Loaded JSON with a http log line from the Zeek log
        source_name (str): Name of the data source

    Returns:
        str: Full nquads definition for given http node.
    """
    if "host" in http_json:
        hostname_hash = hashlib.sha1(http_json.get("host").replace(':', '_0').encode('utf-8')).hexdigest()
        nquads_mutation = textwrap.dedent("""\
            _:{hostname_id} <source> "{data_source}" .
            _:{hostname_id} <dgraph.type> "Hostname" .
            _:{hostname_id} <ioc> _:ioc-{hostname_hash} .
            _:ioc-{hostname_hash} <source> "{data_source}" .
            _:ioc-{hostname_hash} <dgraph.type> "Ioc" .
            _:ioc-{hostname_hash} <ioc.type> "hostname" .
            _:ioc-{hostname_hash} <ioc.value> "{hostname}" .
        """.format(
            data_source=source_name,
            hostname_id="hname-" + hostname_hash,
            hostname_hash=hostname_hash,
            hostname=http_json.get("host"),
        ))
        return textwrap.dedent(nquads_mutation)
    elif "host" in http_json and "uri" in http_json:
        id_http = "http-" + http_json.get("uid", "").replace(':', '_0')
        uri = http_json.get("uri", "").replace('\n', '').replace('\t', '').replace('\r', '').replace('"', '\'').replace('\\', '\\\\')
        hostname_uri_hash = hashlib.sha1((http_json.get("host") + uri).replace(':', '_0').encode('utf-8')).hexdigest()
        nquads_mutation = textwrap.dedent("""\
            _:{id_http} <source> "{data_source}" .
            _:{id_http} <dgraph.type> "Http" .
            _:{id_http} <ioc> _:ioc-{hostname_uri_hash} .
            _:ioc-{hostname_uri_hash} <source> "{data_source}" .
            _:ioc-{hostname_uri_hash} <dgraph.type> "Ioc" .
            _:ioc-{hostname_uri_hash} <ioc.type> "url" .
            _:ioc-{hostname_uri_hash} <ioc.value> "{hostname_uri}" .
        """.format(
            data_source=source_name,
            id_http=id_http,
            hostname_uri_hash=hostname_uri_hash,
            hostname_uri=http_json.get("host") + uri
        ))
    else:
        return ""
