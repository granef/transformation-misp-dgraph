#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Module to parse conn.log file and create related IoC nodes and edges.
"""


import textwrap     # dedent() function to trim multiline strings


def get_mutations(conn_json: dict, source_name: str) -> str:
    """Parsing of the conn log JSON record and definition of a IoC nodes connected to it.

    Args:
        conn_json (dict): Loaded JSON with a conn log line from the Zeek log
        source_name (str): Name of the data source

    Returns:
        str: Full nquads definition for given conn node.
    """
    nquads_mutation = textwrap.dedent("""\
        _:{orig_h} <source> "{data_source}" .
        _:{orig_h} <dgraph.type> "Host" .
        _:{orig_h} <ioc> _:ioc-{orig_h} .
        _:ioc-{orig_h} <source> "{data_source}" .
        _:ioc-{orig_h} <dgraph.type> "Ioc" .
        _:ioc-{orig_h} <ioc.type> "ip" .
        _:ioc-{orig_h} <ioc.value> "{orig_h}" .
        _:{resp_h} <source> "{data_source}" .
        _:{resp_h} <dgraph.type> "Host" .
        _:{resp_h} <ioc> _:ioc-{resp_h} .
        _:ioc-{resp_h} <source> "{data_source}" .
        _:ioc-{resp_h} <dgraph.type> "Ioc" .
        _:ioc-{resp_h} <ioc.type> "ip" .
        _:ioc-{resp_h} <ioc.value> "{resp_h}" .
    """.format(
        data_source=source_name,
        orig_h=conn_json.get("id.orig_h", "").replace(':', '_0'),
        resp_h=conn_json.get("id.resp_h", "").replace(':', '_0')
    ))
    return textwrap.dedent(nquads_mutation)
