#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2022  Milan Cermak, Institute of Computer Science of Masaryk University
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Communication handler that will search for input files and ensure their correct processing using MISP data transformation script.
"""


import sys          # Common system functions
import os           # OS related functions
import subprocess   # Capability to run subprocesses
import argparse     # Arguments parser
import logging, coloredlogs         # Standard logging functionality with colours functionality


def fix_permissions(path: str) -> None:
    """Set persmissions of the given path to enable write and edit to all users.

    Args:
        path (str): Path to directory for which the permissions should to be changed
    """ 
    os.chmod(path, 0o0777)
    for subdir, _, files in os.walk(path):
        for file in files:
            file_path = os.path.join(subdir, file)
            os.chmod(file_path, 0o0666)


def get_zeek_directory(input_directory: str) -> str:
    """Get directory with Zeek logs in given input_directory path.

    Searches for conn.log file which should be present in the directory.

    Args:
        input_directory (str): Path to diectory where to search for zeek logs.

    Returns:
        str: Path to directory with Zeek logs, None otherwise.
    """
    # Check given input directory and zeek subdirectory
    for directory in [input_directory, input_directory + "/zeek/"]:
        for filename in os.listdir(directory):
            if filename == "conn.log":
                return directory
    return None


def get_misp_directory(input_directory: str) -> str:
    """Get directory with MISP events in given input_directory path.

    Args:
        input_directory (str): Path to diectory where to search for zeek logs.

    Returns:
        str: Path to directory with MISP events, None otherwise.
    """
    # Check given input directory and zeek subdirectory
    for directory in [input_directory, input_directory + "/misp/"]:
        for filename in os.listdir(directory):
            if filename.endswith(".jsonl"):
                return directory
    return None


if __name__ == "__main__":
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", metavar="DIRECTORY_PATH", help="Input data directory path", required=True, default="/data/", type=str)
    parser.add_argument("-o", "--output", metavar="DIRECTORY_PATH", help="Output data directory path", default="granef-transformation/misp/", type=str)
    parser.add_argument("-m", "--mounted", metavar="DIRECTORY_PATH", help="Mounted data directory path", default="/data/", type=str)
    parser.add_argument("-rf", "--rdf_file_name", metavar="FILE_NAME", help="Output RDF file name", default="misp-dgraph.rdf", type=str)
    parser.add_argument("-sf", "--schema_file_name", metavar="FILE_NAME", help="Output schema file name", default="misp-dgraph.schema", type=str)
    parser.add_argument("-s", "--source", metavar="CASE_NAME", help="Case name to store as a source to each node", required=False, default="granef", type=str)
    parser.add_argument("-l", "--log", choices=["debug", "info", "warning", "error", "critical"], help="Log level", required=False, default="INFO")
    args = parser.parse_args()

    # Set logging
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt='%(asctime)s [%(levelname)s]: %(message)s')

    # Process input arguments
    output_files_path = args.mounted + "/" + args.output
    output_rdf_file = output_files_path + "/" + args.rdf_file_name
    if not output_rdf_file.endswith(".rdf"): output_rdf_file += ".rdf"
    output_schema_file = output_files_path + "/" + args.schema_file_name
    if not output_schema_file.endswith(".schema"): output_schema_file += ".schema"
    if args.input.replace("/", "") != args.mounted.replace("/", ""):
        input_files_path = args.mounted + "/" + args.input
    logging.debug("Arguments set to: input = " + input_files_path + ", output = " + output_files_path + ", source = " + args.source)

    # Get directories with Zeek logs and MISP event files
    zeek_directory = get_zeek_directory(input_files_path)
    if not zeek_directory:
        logging.error("Directory with Zeek logs not found")
        sys.exit(1)
    misp_directory = get_misp_directory(input_files_path)
    if not misp_directory:
        logging.error("Directory with MISP events not found")
        sys.exit(1)

    # Create output file directory
    if not os.path.isdir(output_files_path):
        os.makedirs(output_files_path)  # TODO Currently overwrites whatever files are in this directory, if it already exists.

    # Start MISP events transformation
    try:
        subprocess.check_call("python3 /usr/local/bin/granef/misp-dgraph.py" +
            " -l " + args.log.lower() +
            " -ed " + misp_directory +
            " -zd " + zeek_directory +
            " -or " + output_rdf_file +
            " -os " + output_schema_file +
            " -s \"" + args.source + "\"", shell=True)
        logging.debug("Output files successfully generated: " + output_rdf_file + ", " + output_schema_file)
        fix_permissions(output_files_path)

    except subprocess.CalledProcessError as e:
        logging.error("(CalledProcessError) When running misp-dgraph.py (" + str(e) + ").")
    except Exception as e:
        logging.error("When running misp-dgraph.py (" + str(e) + ").")
